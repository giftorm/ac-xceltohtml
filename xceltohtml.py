import os

import pandas
from jinja2 import Environment, FileSystemLoader

class XcelToHtml(object):
    # Konstruktor
    def __init__(self, excel_file='', template_path='', target_html_file=''):
        self.excel_file = excel_file
        self.template_path = template_path
        self.target_html_file = target_html_file

    def read_excel(self):
        data = pandas.read_excel(self.excel_file)
        data.fillna('', inplace=True)
        data.replace('nan', '')
        self.data = data

    def create_html_table(self):
        html_table = self.data.to_html(index=None)
        return html_table

    def get_html_template(self, template_name):
        # template folder
        file_loader = FileSystemLoader(self.template_path)
        env = Environment(loader=file_loader)

        # Load template
        self.template = env.get_template(template_name)

    def create_html_file(self, html_table=None):
        if html_table is None:
            print("You need to provide a html table.")
            exit
        # to save the result
        with open(self.target_html_file, "w", encoding='utf8') as fh:
            output = self.template.render(table_data=html_table)
            fh.write(output)
            fh.close()

        print("{} created successfully!".format(self.target_html_file))


if __name__ == "__main__":
    path = os.path.dirname(os.path.abspath(__file__))
    xcel = XcelToHtml(excel_file='{}\\resources\\excel.xlsx'.format(path),
                      template_path='{}\\templates\\'.format(path),
                      target_html_file='{}\\html\\index.html'.format(path)
                      )
    xcel.read_excel()
    html_table = xcel.create_html_table()
    xcel.get_html_template(template_name='table_template.html')
    xcel.create_html_file(html_table)
